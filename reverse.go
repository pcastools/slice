// Reverse contains functions for reversing the order of entries in a slices of primitives.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ReverseInt returns a new slice whose entries are equal to the entries of S, but in the reverse order.
func ReverseInt(S []int) []int {
	n := len(S)
	T := make([]int, 0, n)
	for i := n - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}

// ReverseInt32 returns a new slice whose entries are equal to the entries of S, but in the reverse order.
func ReverseInt32(S []int32) []int32 {
	n := len(S)
	T := make([]int32, 0, n)
	for i := n - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}

// ReverseInt64 returns a new slice whose entries are equal to the entries of S, but in the reverse order.
func ReverseInt64(S []int64) []int64 {
	n := len(S)
	T := make([]int64, 0, n)
	for i := n - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}
