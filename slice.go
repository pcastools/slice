// Slice contains useful utility functions for working with slices of primitives.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

import (
	"strings"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// CopyStr returns a copy of the given slice.
func CopyStr(S []string) (T []string) {
	if S != nil {
		T = make([]string, len(S))
		copy(T, S)
	}
	return
}

// CopyInt returns a copy of the given slice.
func CopyInt(S []int) (T []int) {
	if S != nil {
		T = make([]int, len(S))
		copy(T, S)
	}
	return
}

// IndexStr returns the first index of s in the slice S, or -1 if s cannot be found.
func IndexStr(S []string, s string) int {
	for i, ss := range S {
		if s == ss {
			return i
		}
	}
	return -1
}

// RemoveStr removes the i-th entry from the slice S.
func RemoveStr(S []string, i int) []string {
	n := len(S)
	if i >= 0 && i < n {
		if i == 0 {
			S = S[1:]
		} else if i == n-1 {
			S = S[:n-1]
		} else {
			copy(S[i:], S[i+1:])
			S = S[:n-1]
		}
	}
	return S
}

// IndexInt returns the first index of s in the slice S, or -1 if s cannot be found.
func IndexInt(S []int, s int) int {
	for i, ss := range S {
		if s == ss {
			return i
		}
	}
	return -1
}

// RemoveInt removes the i-th entry from the slice S.
func RemoveInt(S []int, i int) []int {
	n := len(S)
	if i >= 0 && i < n {
		if i == 0 {
			S = S[1:]
		} else if i == n-1 {
			S = S[:n-1]
		} else {
			copy(S[i:], S[i+1:])
			S = S[:n-1]
		}
	}
	return S
}

// InsertInt inserts the value s at the i-th position in the slice S.
func InsertInt(S []int, i int, s int) []int {
	if i < 0 {
		panic("Illegal index")
	}
	n := len(S)
	m := n + 1
	if i+1 > m {
		m = i + 1
	}
	SS := make([]int, m)
	SS[i] = s
	if i == 0 {
		copy(SS[1:], S)
	} else if i < n {
		copy(SS[:i], S)
		copy(SS[i+1:], S[i:])
	} else {
		copy(SS, S)
	}
	return SS
}

// StringNTimes concatenates N copies of S to create a single string. The separator string sep is placed between elements in the resulting string.
func StringNTimes(S string, sep string, N int) string {
	if N == 0 {
		return ""
	} else if N == 1 {
		return S
	}
	if len(sep) == 0 {
		return strings.Repeat(S, N)
	}
	l := (N-1)*len(sep) + N*len(S)
	b := make([]byte, l)
	bp := copy(b, S)
	S = sep + S
	for i := 1; i < N; i++ {
		bp += copy(b[bp:], S)
	}
	return string(b)
}
