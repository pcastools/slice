// Explode contains tools for converting strings to slices of primitives.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

import (
	"fmt"
	"io"
	"math/big"
	"strconv"
	"strings"
	"unicode"
)

// Sep defines the separator used when splitting strings
const Sep = ','

// The maximum number of digits (not including a -) in a string representation
// of an integer of the indicated type.
const (
	lenInt8   = 3
	lenInt16  = 5
	lenInt32  = 10
	lenInt64  = 19
	lenUint8  = 3
	lenUint16 = 5
	lenUint32 = 10
	lenUint64 = 20
)

// Error messages.
const (
	MsgFailedToConvertIndex = "Failed to convert entry at index %d."
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// bigIntFromString attempts to convert the given string to a *big.Int.
func bigIntFromString(s string) (val *big.Int, err error) {
	t := new(big.Int)
	if _, err = fmt.Sscan(strings.TrimSpace(s), t); err == nil {
		val = t
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Slice from string
/////////////////////////////////////////////////////////////////////////

// IntSliceFromString converts a string into a slice of ints (i.e. []int).
func IntSliceFromString(str string) ([]int, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]int, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, lenInt())
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]int, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n int
		if n, err = strconv.Atoi(c); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Int8SliceFromString converts a string into a slice of int8s (i.e. []int8).
func Int8SliceFromString(str string) ([]int8, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]int8, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, lenInt8)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]int8, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n int64
		if n, err = strconv.ParseInt(c, 10, 8); err == nil {
			vals = append(vals, int8(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Int16SliceFromString converts a string into a slice of int16s (i.e. []int16).
func Int16SliceFromString(str string) ([]int16, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]int16, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, lenInt16)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]int16, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n int64
		if n, err = strconv.ParseInt(c, 10, 16); err == nil {
			vals = append(vals, int16(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Int32SliceFromString converts a string into a slice of int32s (i.e. []int32).
func Int32SliceFromString(str string) ([]int32, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]int32, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, lenInt32)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]int32, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n int64
		if n, err = strconv.ParseInt(c, 10, 32); err == nil {
			vals = append(vals, int32(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Int64SliceFromString converts a string into a slice of int64s (i.e. []int64).
func Int64SliceFromString(str string) ([]int64, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]int64, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, lenInt64)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]int64, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n int64
		if n, err = strconv.ParseInt(c, 10, 64); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// UintSliceFromString converts a string into a slice of uints (i.e. []uint).
func UintSliceFromString(str string) ([]uint, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]uint, 0), nil
	}
	// Validate the string
	ok, length := AreUnsignedIntegers(str, Sep, lenUint())
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]uint, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n uint64
		if n, err = strconv.ParseUint(c, 10, 0); err == nil {
			vals = append(vals, uint(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Uint8SliceFromString converts a string into a slice of uint8s (i.e. []uint8).
func Uint8SliceFromString(str string) ([]uint8, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]uint8, 0), nil
	}
	// Validate the string
	ok, length := AreUnsignedIntegers(str, Sep, lenUint8)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]uint8, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n uint64
		if n, err = strconv.ParseUint(c, 10, 8); err == nil {
			vals = append(vals, uint8(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Uint16SliceFromString converts a string into a slice of uint16s (i.e. []uint16).
func Uint16SliceFromString(str string) ([]uint16, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]uint16, 0), nil
	}
	// Validate the string
	ok, length := AreUnsignedIntegers(str, Sep, lenUint16)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]uint16, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n uint64
		if n, err = strconv.ParseUint(c, 10, 16); err == nil {
			vals = append(vals, uint16(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Uint32SliceFromString converts a string into a slice of uint32s (i.e. []uint32).
func Uint32SliceFromString(str string) ([]uint32, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]uint32, 0), nil
	}
	// Validate the string
	ok, length := AreUnsignedIntegers(str, Sep, lenUint32)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]uint32, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n uint64
		if n, err = strconv.ParseUint(c, 10, 32); err == nil {
			vals = append(vals, uint32(n))
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Uint64SliceFromString converts a string into a slice of uint64s (i.e. []uint64).
func Uint64SliceFromString(str string) ([]uint64, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]uint64, 0), nil
	}
	// Validate the string
	ok, length := AreUnsignedIntegers(str, Sep, lenUint64)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]uint64, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n uint64
		if n, err = strconv.ParseUint(c, 10, 64); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// Float64SliceFromString converts a string into a slice of float64s (i.e. []float64).
func Float64SliceFromString(str string) ([]float64, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]float64, 0), nil
	}
	// Create the destination slice and splitter
	length := strings.Count(str, string(Sep))
	vals := make([]float64, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n float64
		if n, err = strconv.ParseFloat(c, 64); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// BoolSliceFromString converts a string into a slice of bools (i.e. []bool).
func BoolSliceFromString(str string) ([]bool, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]bool, 0), nil
	}
	// Create the destination slice and splitter
	length := strings.Count(str, string(Sep))
	vals := make([]bool, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n bool
		if n, err = strconv.ParseBool(c); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// BigIntSliceFromString converts a string into a slice of *big.Ints (i.e. []*big.Int).
func BigIntSliceFromString(str string) ([]*big.Int, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]*big.Int, 0), nil
	}
	// Validate the string
	ok, length := AreIntegers(str, Sep, 0)
	if !ok {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]*big.Int, 0, length)
	S := DefaultStringSplitter(str, Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n *big.Int
		if n, err = bigIntFromString(c); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

/////////////////////////////////////////////////////////////////////////
// Slices from string
/////////////////////////////////////////////////////////////////////////

// Int64SlicesFromString returns str as a slice of slices of int64s (i.e. [][]int64).
func Int64SlicesFromString(str string) ([][]int64, error) {
	// Normalize the string
	str = TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return [][]int64{}, nil
	}
	// Create an empty slice to hold the results
	entries := make([][]int64, 0, 16)
	for len(str) != 0 {
		// Note and validate the opening bracket
		open := str[0]
		if open != '(' && open != '[' && open != '{' {
			return nil, ErrInvalidRune
		}
		// Move forwards to the next bracket
		str = str[1:]
		idx := strings.IndexAny(str, "([{)]}")
		if idx == -1 {
			return nil, ErrUnbalancedBraces
		}
		// This had better be the corresponding closing bracket
		next := str[idx]
		if next == '(' || next == '[' || next == '{' {
			return nil, ErrInvalidRune
		} else if (open == '(' && next != ')') || (open == '[' && next != ']') || (open == '{' && next != '}') {
			return nil, ErrUnbalancedBraces
		}
		// Convert the entry
		if pt, err := Int64SliceFromString(str[:idx]); err == nil {
			entries = append(entries, pt)
		} else {
			return nil, err
		}
		// Move on
		str = strings.TrimLeftFunc(str[idx+1:], unicode.IsSpace)
		if len(str) != 0 {
			if str[0] == defaultSplitRune {
				str = strings.TrimLeftFunc(str[1:], unicode.IsSpace)
				if len(str) == 0 {
					return nil, ErrUnexpectedEndOfString
				}
			} else {
				return nil, ErrInvalidRune
			}
		}
	}
	// Looks good
	return entries, nil
}
