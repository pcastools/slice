// String contains useful utility functions for working with string representations of slices.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

import (
	"strings"
	"unicode"
)

// Panic messages.
const (
	msgIllegalSep = "Illegal separator."
	msgMaxLen     = "Invalid maximum length."
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SplitAndTrim returns splits the string s using the separator sep, and trims any whitespace from the resulting substrings. Only non-empty substrings will be included in the returned slice.
func SplitAndTrim(s string, sep string) (T []string) {
	// Is there anything to do?
	if s = strings.TrimSpace(s); len(s) == 0 {
		return
	}
	// Create a new slice
	T = make([]string, 0, 5)
	// Split the string and add any non-empty substrings to the slice
	for _, t := range strings.Split(s, sep) {
		if tt := strings.TrimSpace(t); len(tt) > 0 {
			T = append(T, tt)
		}
	}
	return
}

// TrimSpaceAndSurroundingBrackets removes initial and training white-space and at most one matching pair of brackets from the string. White-space is as defined by unicode.IsSpace, valid pairs of matching brackets are '(',')' and '{','}' and '[',']'.
func TrimSpaceAndSurroundingBrackets(s string) string {
	// Trim the white-space
	s = strings.TrimSpace(s)
	// Is there anything to do?
	n := len(s)
	if n == 0 {
		return s
	}
	// Remove leading and trailing brackets, if any, and trim the white-space
	if (s[0] == '(' && s[n-1] == ')') || (s[0] == '{' && s[n-1] == '}') || (s[0] == '[' && s[n-1] == ']') {
		s = strings.TrimSpace(s[1 : n-1])
	}
	return s
}

// AreIntegers performs a check that the given string is of the form
//    [[:space:]*[-][:space:]*[0-9,:space:]+[:space:]*:sep:]*
// i.e. represents a sequence of integers separated by the rune 'sep'. The "maxLen" argument specifies the maximum number of digits allowed in an integer; set to 0 to impose no limit. Returns true followed by the slice length on success, false followed by the index (in the slice) of the first malformed entry otherwise. (A rune is defined to be white-space if it satisfies unicode.IsSpace. If 'sep' is white-space, a digit, or '-' then a panic occurs.)
func AreIntegers(s string, sep rune, maxLen int) (bool, int) {
	// Sanity check
	if sep == '-' || unicode.IsDigit(sep) || unicode.IsSpace(sep) {
		panic(msgIllegalSep)
	}
	if maxLen < 0 {
		panic(msgMaxLen)
	}
	// Start parsing the string
	first := true
	block := 0
	length := 0
	if idx := strings.IndexFunc(s, func(r rune) bool {
		if unicode.IsSpace(r) {
			return false
		}
		if r == '-' {
			if !first {
				return true
			}
			first = false
			return false
		}
		if unicode.IsDigit(r) {
			first = false
			length++
			if maxLen != 0 && length > maxLen {
				return true
			}
			return false
		}
		if r == sep {
			block++
			first = true
			length = 0
			return false
		}
		return true
	}); idx != -1 {
		return false, block
	}
	// Looks good
	return true, block + 1
}

// AreUnsignedIntegers performs a check that the given string is of the form
//    [[:space:]*[0-9,:space:]+[:space:]*:sep:]*
// i.e. represents a sequence of unsigned integers separated by the rune 'sep'. The "maxLen" argument specifies the maximum number of digits allowed in an integer; set to 0 to impose no limit. Returns true followed by the slice length on success, false followed by the index (in the slice) of the first malformed entry otherwise. (A rune is defined to be white-space if it satisfies unicode.IsSpace.)
func AreUnsignedIntegers(s string, sep rune, maxLen int) (bool, int) {
	// Sanity check
	if maxLen < 0 {
		panic(msgMaxLen)
	}
	// Start parsing the string
	block := 0
	length := 0
	if idx := strings.IndexFunc(s, func(r rune) bool {
		if unicode.IsSpace(r) {
			return false
		}
		if unicode.IsDigit(r) {
			length++
			if maxLen != 0 && length > maxLen {
				return true
			}
			return false
		}
		if r == sep {
			block++
			length = 0
			return false
		}
		return true
	}); idx != -1 {
		return false, block
	}
	// Looks good
	return true, block + 1
}
