// Join contains tools for converting slices of primitives to a string.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

import (
	"math"
	"math/big"
	"strconv"
	"strings"
)

// The maximum size of an integer
const maxInt = int64(int(^uint(0) >> 1))

// The maximum size of an unsigned integer
const maxUint = uint64(^uint(0))

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// lenInt returns the maximum number of digits (not including a -) in a string representation of an int.
func lenInt() int {
	if maxInt == math.MaxInt32 {
		return lenInt32
	}
	return lenInt64
}

// lenUint returns the maximum number of digits (not including a -) in a string representation of a uint.
func lenUint() int {
	if maxUint == math.MaxUint32 {
		return lenUint32
	}
	return lenUint64
}

// intLength returns the number of bytes needed to express the given integer as a string.
func intLength(n int) int {
	l := 1
	if n < 0 {
		n *= -1
		l++
	}
	for n >= 10 {
		n /= 10
		l++
	}
	return l
}

// int64Length returns the number of bytes needed to express the given integer as a string.
func int64Length(n int64) int {
	l := 1
	if n < 0 {
		n *= -1
		l++
	}
	for n >= 10 {
		n /= 10
		l++
	}
	return l
}

// uintLength returns the number of bytes needed to express the given unsigned integer as a string.
func uintLength(n uint) int {
	l := 1
	for n >= 10 {
		n /= 10
		l++
	}
	return l
}

// uint64Length returns the number of bytes needed to express the given unsigned integer as a string.
func uint64Length(n uint64) int {
	l := 1
	for n >= 10 {
		n /= 10
		l++
	}
	return l
}

/////////////////////////////////////////////////////////////////////////
// Slice to string
/////////////////////////////////////////////////////////////////////////

// JoinIntSlice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinIntSlice(S []int, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += intLength(n)
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.Itoa(n))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.Itoa(n))
		}
	}
	return string(b)
}

// JoinInt8Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinInt8Slice(S []int8, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += intLength(int(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.Itoa(int(n)))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.Itoa(int(n)))
		}
	}
	return string(b)
}

// JoinInt16Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinInt16Slice(S []int16, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += intLength(int(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.Itoa(int(n)))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.Itoa(int(n)))
		}
	}
	return string(b)
}

// JoinInt32Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinInt32Slice(S []int32, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += intLength(int(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.Itoa(int(n)))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.Itoa(int(n)))
		}
	}
	return string(b)
}

// JoinInt64Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinInt64Slice(S []int64, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += int64Length(n)
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatInt(n, 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatInt(n, 10))
		}
	}
	return string(b)
}

// JoinUintSlice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinUintSlice(S []uint, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += uintLength(n)
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatUint(uint64(n), 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatUint(uint64(n), 10))
		}
	}
	return string(b)
}

// JoinUint8Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinUint8Slice(S []uint8, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += uintLength(uint(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatUint(uint64(n), 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatUint(uint64(n), 10))
		}
	}
	return string(b)
}

// JoinUint16Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinUint16Slice(S []uint16, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += uintLength(uint(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatUint(uint64(n), 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatUint(uint64(n), 10))
		}
	}
	return string(b)
}

// JoinUint32Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinUint32Slice(S []uint32, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += uintLength(uint(n))
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatUint(uint64(n), 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatUint(uint64(n), 10))
		}
	}
	return string(b)
}

// JoinUint64Slice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string.
func JoinUint64Slice(S []uint64, sep string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, n := range S {
		l += uint64Length(n)
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, n := range S {
		if first {
			bp = copy(b, strconv.FormatUint(n, 10))
			first = false
		} else {
			bp += copy(b[bp:], sep)
			bp += copy(b[bp:], strconv.FormatUint(n, 10))
		}
	}
	return string(b)
}

// JoinBoolSlice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string.
func JoinBoolSlice(S []bool, sep string) string {
	if len(S) == 0 {
		return ""
	}
	b := make([]byte, len(sep)*(len(S)-1)+len(S))
	first := true
	var bp int
	for _, x := range S {
		if first {
			if x {
				bp = copy(b, "1")
			} else {
				bp = copy(b, "0")
			}
			first = false
		} else {
			bp += copy(b[bp:], sep)
			if x {
				bp += copy(b[bp:], "1")
			} else {
				bp += copy(b[bp:], "0")
			}
		}
	}
	return string(b)
}

// JoinBoolSliceWithNames concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string. The string trueStr will be used when a boolean is true; the string falseStr will be used when a boolean is false.
func JoinBoolSliceWithNames(S []bool, sep string, trueStr string, falseStr string) string {
	if len(S) == 0 {
		return ""
	}
	l := len(sep) * (len(S) - 1)
	for _, x := range S {
		if x {
			l += len(trueStr)
		} else {
			l += len(falseStr)
		}
	}
	b := make([]byte, l)
	first := true
	var bp int
	for _, x := range S {
		if first {
			if x {
				bp = copy(b, trueStr)
			} else {
				bp = copy(b, falseStr)
			}
			first = false
		} else {
			bp += copy(b[bp:], sep)
			if x {
				bp += copy(b[bp:], trueStr)
			} else {
				bp += copy(b[bp:], falseStr)
			}
		}
	}
	return string(b)
}

// JoinBigIntSlice concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string
func JoinBigIntSlice(S []*big.Int, sep string) string {
	if len(S) == 0 {
		return ""
	}
	T := make([]string, 0, len(S))
	for _, s := range S {
		T = append(T, s.String())
	}
	return strings.Join(T, sep)
}
