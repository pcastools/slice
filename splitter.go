// Splitter provides a convenient way of scanning an io.Reader and splitting it at specified runes.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package slice

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"strings"
	"unicode"
)

// Splitter provides a convenient way of scanning an io.Reader and splitting it at specified runes.
type Splitter struct {
	// IsEscape is an optional function that should return true if the given rune is an escape rune. If true, the next rune will be escaped and added verbatim (along with the escape rune) to the returned substring. If io.EOF is reached whilst escaped, an ErrEOFWhilstEscaped error will be returned by Next().
	IsEscape func(ch rune) bool
	// Ignore is an optional function that can be be used to ignored a rune when scanning the stream. Ignored runes will be skipped-over and not form part of any returned substrings. Ignore should return true if the given rune should be ignored. The escape rune, or a rune preceded by the escape rune, will not be considered.
	Ignore func(ch rune) bool
	// SplitOn is an optional function that can be used to specify the end of a substring (the split rune itself will not be included). If notspecified, splits will occur on ','. Ignored runes, the escape rune, or runes preceded by the escape rune, will not be considered.
	SplitOn func(ch rune) bool
	// IsValid is an optional function that can be used to validate the stream. It should return true if the given rune is valid, false otherwise. If IsValid returns false, then the Splitter's Next() function will return an ErrInvalidRune error. Split runes, ignored runes, the escape rune, or runes preceded by the escape rune, will not be considered.
	IsValid func(ch rune) bool
	// Set TrimSpace to true to have consecutive white-space (as defined by unicode.IsSpace) trimmed from the start and end of each substring. A rune will only be considered if it is valid, is not ignored, has not been escaped, and is not used for splitting the stream into substrings.
	TrimSpace bool
	// Set CollateSpace to true to have consecutive white-space (as defined by unicode.IsSpace) to a single ' '. A rune will only be considered if it is valid, is not ignored, has not been escaped, and is not used for splitting the stream into substrings.
	CollateSpace bool
	// Set MaxLength to the maximum length of a substring (or the default of for no limit beyond that imposed by bytes.Buffer). If the maximum length is exceeded, Next() will return an ErrTooLarge error.
	MaxLength int
	// The underlying source
	src io.RuneReader
	// The buffer into which we read the substrings
	buf *bytes.Buffer
	// The index of the next rune in the stream to be read.
	idx int
	// Have we reached the end of the stream?
	eof bool
	// Are we currently escaped?
	isEscaped bool
	// Are we currently parsing white-space?
	inSpace bool
	// Are we currently trimming white-space from the start of the string?
	areTrimming bool
	// The length of the buffer after writing the last non-white-space rune
	nonSpaceLen int
}

// Common errors.
var (
	ErrUninitializedSplitter = errors.New("Uninitialized splitter")
	ErrInvalidRune           = errors.New("Invalid rune")
	ErrEOFWhilstEscaped      = errors.New("Unexpected EOF whilst in an escaped sequence")
	ErrTooLarge              = errors.New("Substring is too large")
	ErrUnbalancedBraces      = errors.New("Unbalanced braces")
	ErrUnexpectedEndOfString = errors.New("Unexpected end of string")
)

// The default split rune
const defaultSplitRune = ','

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// parseRune handles the given rune. Returns true if the stream should be split at this rune. May also return an error.
func (s *Splitter) parseRune(r rune) (split bool, err error) {
	// We need to make a note as to whether we're trimming white-space
	areTrimming := s.areTrimming
	if areTrimming {
		s.areTrimming = false
	}
	// Are we escaped?
	if s.isEscaped {
		s.isEscaped = false
		s.buf.WriteRune(r)
		s.nonSpaceLen = s.buf.Len()
		return
	}
	// Is this an escape rune?
	if s.IsEscape != nil && s.IsEscape(r) {
		s.isEscaped = true
		s.inSpace = false
		s.buf.WriteRune(r)
		s.nonSpaceLen = s.buf.Len()
		return
	}
	// Should we ignore this rune?
	if s.Ignore != nil && s.Ignore(r) {
		return
	}
	// Should we split on this rune?
	if s.SplitOn != nil {
		if s.SplitOn(r) {
			split = true
			return
		}
	} else if r == defaultSplitRune {
		split = true
		return
	}
	// Is this rune valid?
	if s.IsValid != nil && !s.IsValid(r) {
		err = ErrInvalidRune
		return
	}
	// Are we trimming white-space from the beginning of the string?
	isSpace := unicode.IsSpace(r)
	if areTrimming && isSpace {
		s.areTrimming = true
		return
	}
	// Are we collating white-space?
	if s.CollateSpace {
		if isSpace {
			if !s.inSpace {
				s.inSpace = true
				s.buf.WriteRune(' ')
			}
			return
		}
		s.inSpace = false
	}
	// If we're here then we add the rune to the buffer and return
	if isSpace {
		s.buf.WriteRune(r)
	} else {
		s.buf.WriteRune(r)
		s.nonSpaceLen = s.buf.Len()
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Splitter functions
/////////////////////////////////////////////////////////////////////////

// Init initializes a splitter with the given source and returns s.
func (s *Splitter) Init(src io.Reader) *Splitter {
	if s != nil {
		if rr, ok := src.(io.RuneReader); ok {
			s.src = rr
		} else {
			s.src = bufio.NewReader(src)
		}
		if s.buf == nil {
			s.buf = new(bytes.Buffer)
		}
		s.idx = 0
		s.eof = false
	}
	return s
}

// Index returns the index of the next rune in the stream to be read. Note that this count includes ignored runes, split runes, etc. hence may be larger the the total given by the substrings.
func (s *Splitter) Index() int {
	if s == nil || s.src == nil {
		return 0
	}
	return s.idx
}

// Next returns the next substring in the stream, or an empty string followed by an error (which, at the end of the stream, will be io.EOF).
func (s *Splitter) Next() (string, error) {
	// Have we been initialized? Have we reached the end of the stream?
	if s == nil || s.src == nil {
		return "", ErrUninitializedSplitter
	}
	if s.eof {
		return "", io.EOF
	}
	// Initialize the memory ready for this substring
	s.inSpace = false
	s.isEscaped = false
	s.areTrimming = s.TrimSpace
	s.nonSpaceLen = 0
	// Clear the buffer on return
	defer s.buf.Truncate(0)
	// Read in until the next split
	split := false
	for !split {
		// Fetch the next rune
		if r, _, err := s.src.ReadRune(); err == nil {
			// Increment the index
			s.idx++
			// How should we respond to this rune?
			if split, err = s.parseRune(r); err != nil {
				return "", err
			}
			// Have we exceeded the maximum length?
			if s.MaxLength != 0 &&
				((s.TrimSpace && s.nonSpaceLen > s.MaxLength) ||
					(!s.TrimSpace && s.buf.Len() > s.MaxLength)) {
				return "", ErrTooLarge
			}
		} else if err == io.EOF {
			// Mark ourselves as at eof
			s.eof = true
			// If we are escaped then we error, otherwise split
			if s.isEscaped {
				return "", ErrEOFWhilstEscaped
			}
			split = true
		} else {
			// We've encountered an error reading from the stream
			return "", err
		}
	}
	// Do we need to trim white-space from the end of the string?
	if s.TrimSpace {
		s.buf.Truncate(s.nonSpaceLen)
	}
	// Return the substring
	return s.buf.String(), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultStringSplitter returns a new splitter for the given string. It will split on the seperator sep, automatically removing all white-space (as determined by univode.IsSpace) as it goes.
func DefaultStringSplitter(str string, sep rune) *Splitter {
	S := new(Splitter)
	S.Init(strings.NewReader(str))
	S.Ignore = unicode.IsSpace
	S.SplitOn = func(r rune) bool { return r == sep }
	return S
}
